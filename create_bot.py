from aiogram import Bot
from aiogram.dispatcher import Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

from os import environ

"""
Creating bot and dispatcher instances
"""

storage = MemoryStorage()

token_tg = environ.get("TEST_BOT_TOKEN")

bot = Bot(token=token_tg)
dp = Dispatcher(bot, storage=storage)
