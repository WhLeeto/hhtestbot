import asyncio

import aiohttp
import requests


def get_dogo() -> str or False:
    """
    Geting random dogo pic.
    :return: pic url
    """
    url = "https://random.dog/woof.json"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()["url"]
    else:
        return False


async def get_dogo_async():
    """
    Example of async request for photo.
    """
    async with aiohttp.ClientSession() as session:
        async with session.get('https://random.dog/woof.json') as resp:
            if resp.status == 200:
                await resp.json()
            else:
                return

