import requests


def get_currency_list() -> str or False:
    """
    Getting currency states json.
    """
    url = "https://www.cbr-xml-daily.ru/daily_json.js"
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()
    else:
        return False