from os import environ

import requests
from transliterate import translit


# def get_weather(city: str) -> dict or False:
#     url = "https://openweather43.p.rapidapi.com/weather"
#     headers = {
#         "X-RapidAPI-Key": "374a984c5cmshe2746338a18bd1dp17e7e6jsnf5c696f83819",
#         "X-RapidAPI-Host": "openweather43.p.rapidapi.com"
#     }
#     params = {
#         "q": {city},
#         "appid": "da0f9c8d90bde7e619c3ec47766a42f4",
#         "units": "metric"
#     }
#     response = requests.get(url, headers=headers, params=params)
#     if response.status_code == 200:
#         return response.json()
#     else:
#         print(response)
#         return False


def get_weather_from_openweathermap(city: str) -> dict or False:
    """
    Request to openweathermap
    """
    url = "https://api.openweathermap.org/data/2.5/weather"
    params = {
        "q": {city},
        "appid": environ.get("WEATHER_TOKEN"),
        "units": "metric"
    }
    resp = requests.get(url, params=params)
    if resp.status_code == 200:
        return resp.json()
    else:
        return False
