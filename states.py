from aiogram.dispatcher.filters.state import StatesGroup, State


class FSM_weather(StatesGroup):
    """
    States for weather command
    """
    step_1 = State()
    step_2 = State()


class FSM_survey(StatesGroup):
    """
    States for survey command
    """
    enter_question = State()
    commit_data = State()
    send_survey = State()


class FSM_currency(StatesGroup):
    """
    States for currency command
    """
    enter_amount = State()
    get_result = State()