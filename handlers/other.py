import asyncio

from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher

from api_requests.currency import get_currency_list
from api_requests.dogos import get_dogo
from api_requests.weather import get_weather_from_openweathermap
from create_bot import dp, bot
from handlers import weather, currency, survey
from keyboards.inline_kb_generator import create_kb
from states import FSM_survey, FSM_weather, FSM_currency
from variables import var


async def get_picture(cq: types.CallbackQuery):
    """
    Catching "picture" callback and asking api about doggo picture.
    """
    await cq.answer()
    message = await cq.message.answer("Обрабатываю запрос ...")
    photo = get_dogo()
    if photo:
        await bot.delete_message(chat_id=cq.from_user.id, message_id=cq.message.message_id)
        await bot.edit_message_text(text="Вот симпотичная собачка: ", chat_id=cq.from_user.id,
                                    message_id=message.message_id, reply_markup=None)
        await bot.send_photo(chat_id=cq.from_user.id, photo=photo)
    else:
        await bot.delete_message(chat_id=cq.from_user.id, message_id=cq.message.message_id)
        await bot.edit_message_text(text="Нет ответа от API T_T", chat_id=cq.from_user.id,
                                    message_id=message.message_id, reply_markup=None)


def register_handlers_other(dp: Dispatcher):
    weather.register_handlers_weather(dp)
    currency.register_handlers_currency(dp)
    survey.register_handlers_survey(dp)

    dp.register_callback_query_handler(get_picture, lambda c: c.data == "picture")




