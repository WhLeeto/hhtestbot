from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext

from api_requests.currency import get_currency_list
from create_bot import bot
from keyboards.inline_kb_generator import create_kb
from states import FSM_currency


async def get_currency(cq: types.CallbackQuery, state: FSMContext):
    """
    Catching "currency" callback.
    """
    await cq.answer()
    await bot.edit_message_reply_markup(chat_id=cq.from_user.id, message_id=cq.message.message_id, reply_markup=None)
    await FSM_currency.enter_amount.set()
    to_del = await cq.message.answer("В какую валюту будем переводить рубли?",
                                     reply_markup=create_kb(text=['Доллары', 'Евро', 'Теньге'],
                                                            cq=['currency usd', 'currency eur', 'currency kzt'],
                                                            rows=1))
    async with state.proxy() as data:
        data["to_del"] = to_del.message_id


async def enter_amount(cq: types.CallbackQuery, state: FSMContext):
    """
    Catching type of curruncy.
    """
    await cq.answer()
    async with state.proxy() as data:
        data["currency_type"] = cq.data.split(" ")[1]
        print(data["currency_type"])
        await bot.edit_message_text(text="Введите сумму в рублях:", chat_id=cq.from_user.id,
                                    message_id=data["to_del"])
    await FSM_currency.get_result.set()


async def get_result(msg: types.Message, state: FSMContext):
    """
    Catching amount and sending api request
    """
    if msg.text.isdigit():
        currency_dict = get_currency_list()
        if currency_dict:
            async with state.proxy() as data:
                if data["currency_type"] == "usd":
                    course = float(currency_dict["Valute"]["USD"]["Value"])
                    name = "долларов"
                elif data["currency_type"] == "eur":
                    course = float(currency_dict["Valute"]["EUR"]["Value"])
                    name = "евро"
                elif data["currency_type"] == "kzt":
                    course = float(currency_dict["Valute"]["KZT"]["Value"] / 100)
                    name = "теньге"
            text = f"{msg.text} рублей сейчас около {round(float(msg.text) / course, 1)} {name}\n" \
                   f"Текущий курс {name} ~ {round(course, 2)}"
            await msg.answer(text)
            await state.finish()
        else:
            await msg.answer("Апи с валютой не работает, ничего не могу поделать")
            await state.finish()
    else:
        await msg.answer("Необходимо ввести целое число")


def register_handlers_currency(dp: Dispatcher):
    dp.register_callback_query_handler(get_currency, lambda c: c.data == "currency")
    dp.register_callback_query_handler(enter_amount,
                                       lambda c: c.data.startswith("currency"), state=FSM_currency.enter_amount)
    dp.register_message_handler(get_result, state=FSM_currency.get_result)