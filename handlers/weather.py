from aiogram import Dispatcher, types
from aiogram.dispatcher import FSMContext

from api_requests.weather import get_weather_from_openweathermap
from create_bot import bot
from states import FSM_weather


async def get_weather(cq: types.CallbackQuery):
    """
    Catching "weather" callback.
    """
    await cq.answer()
    await FSM_weather.step_1.set()
    await bot.edit_message_text(text="В каком городе хотите узнать погоду ?",
                                chat_id=cq.from_user.id,
                                message_id=cq.message.message_id)


async def send_weather(msg: types.Message, state: FSMContext):
    """
    Catching city from user and trying to send api request to weather api.
    """
    weather = get_weather_from_openweathermap(city=msg.text)
    if weather:
        text = f"<b>{weather['name']}</b>\n" \
               f"Температура: {weather['main']['temp_max']}\n" \
               f"Ощущается как: {weather['main']['feels_like']}\n" \
               f"Давление: {weather['main']['pressure']}\n" \
               f"Влажность: {weather['main']['humidity']}\n"
        await msg.answer(text, parse_mode=types.ParseMode.HTML)
    else:
        await msg.answer("Бот не смог найти погоду по вашему запросу")
    await state.finish()


def register_handlers_weather(dp: Dispatcher):
    dp.register_callback_query_handler(get_weather, lambda c: c.data == "weather")
    dp.register_message_handler(send_weather, state=FSM_weather.step_1)