from aiogram import Dispatcher
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram import types, Dispatcher
from create_bot import dp, bot
import asyncio
from aiogram.utils.exceptions import CantInitiateConversation

from keyboards.inline_main_choose import main_choose_kb


async def comand_start(msg: types.Message):
    """
    Just a start command.
    """
    text = "<b>Приветствую путник!</b>\n\n" \
           "Обязательно присоеденись к тестовой <a href='https://t.me/+WFatn9NTDEowOGJi'>группе</a>!\n" \
           "Что заставило тебя обратиться ко мне?"
    if msg.chat.type == types.ChatType.PRIVATE:
        await msg.answer(text,
                         reply_markup=main_choose_kb, parse_mode=types.ParseMode.HTML, disable_web_page_preview=True)
    else:
        try:
            await msg.answer("Ответил в личку ;)")
            await bot.send_message(chat_id=msg.from_user.id, text=text, reply_markup=main_choose_kb,
                                   parse_mode=types.ParseMode.HTML)
        except CantInitiateConversation:
            await msg.reply("Я стеснительный и не могу написать в личку первым. Придется тебе начать диалог со мной")


async def test(msg: types.Message):
    await msg.answer("Бот работает\n"
                     f"Твой id: <code>{msg.from_user.id}</code>\n"
                     f"Id руппы: <code>{msg.chat.id}</code>", parse_mode=types.ParseMode.HTML)


def register_handlers_client(dp: Dispatcher):
    dp.register_message_handler(comand_start, commands=["start", "help"])
    dp.register_message_handler(test, commands="test")
