from aiogram import types, Dispatcher
from aiogram.dispatcher import FSMContext

from create_bot import bot
from keyboards.inline_kb_generator import create_kb
from states import FSM_survey
from variables import var


async def start_survey(cq: types.CallbackQuery, state: FSMContext):
    """
    Catching "survey" callback.
    """
    await FSM_survey.enter_question.set()
    to_del = await bot.edit_message_text(text="Давай создадим опрос.\n\n"
                                              "Напиши сам вопрос:",
                                         chat_id=cq.from_user.id,
                                         message_id=cq.message.message_id, )
    async with state.proxy() as data:
        data["to_del"] = to_del


async def enter_answers(msg: types.Message, state: FSMContext):
    """
    Catching survey name.
    """
    await msg.delete()
    async with state.proxy() as data:
        data["question"] = msg.text
        await FSM_survey.commit_data.set()
        to_del = await bot.edit_message_text(text="Теперь введи варианты ответа через / "
                                                  "(Например: Первый вариант /Второй вариант)",
                                             chat_id=msg.from_user.id,
                                             message_id=data["to_del"].message_id)
        data["to_del"] = to_del


async def commit_data(msg: types.Message, state: FSMContext):
    """
    Catching answers.
    """
    await FSM_survey.send_survey.set()
    await msg.delete()
    answers_list = [i for i in msg.text.split("/")]
    answers_text = ""
    num = 0
    for i in answers_list:
        num += 1
        answers_text += f"{num}. {i}\n"
    async with state.proxy() as data:
        data["answers_list"] = answers_list
        to_del = await bot.edit_message_text(text="Давай проверим что получилось:",
                                             chat_id=msg.from_user.id,
                                             message_id=data["to_del"].message_id)
        to_edit = await msg.answer(f"Название опроса: {data['question']}\n"
                                   f"Варианты ответа:\n{answers_text}\n\n"
                                   f"Отправляем в группу?", reply_markup=create_kb(text=["Да", "Нет"],
                                                                                   cq=["surv y", "surv n"],
                                                                                   rows=2))
        data["to_del"] = to_del
        data["to_edit"] = to_edit


async def send_survey(cq: types.CallbackQuery, state: FSMContext):
    """
    Submitting and sending poll.
    """
    async with state.proxy() as data:
        await data["to_del"].delete()
    if cq.data.split(" ")[1] == "y":
        await bot.edit_message_text(text="Опрос отправлен",
                                    chat_id=cq.from_user.id,
                                    message_id=data["to_edit"].message_id)
        async with state.proxy() as data:
            await bot.send_poll(chat_id=var["group_chat_id"],
                                question=data["question"],
                                options=data["answers_list"])
    elif cq.data.split(" ")[1] == "n":
        await bot.edit_message_text(text="Опрос не отправлен",
                                    chat_id=cq.from_user.id,
                                    message_id=data["to_edit"].message_id)
    await state.finish()


def register_handlers_survey(dp: Dispatcher):
    dp.register_callback_query_handler(start_survey, lambda c: c.data == "survey")
    dp.register_message_handler(enter_answers, state=FSM_survey.enter_question)
    dp.register_message_handler(commit_data, state=FSM_survey.commit_data)
    dp.register_callback_query_handler(send_survey, lambda c: c.data.startswith("surv"), state=FSM_survey.send_survey)
