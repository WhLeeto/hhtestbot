from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

"""
Example of keyboard creating.
"""

button_1 = InlineKeyboardButton(text="Определить текущую погоду", callback_data="weather")
button_2 = InlineKeyboardButton(text="Конвертировать валюты", callback_data="currency")
button_3 = InlineKeyboardButton(text="Случайная картинка", callback_data="picture")
button_4 = InlineKeyboardButton(text="Создать опрос", callback_data="survey")
main_choose_kb = InlineKeyboardMarkup(row_width=1).add(button_1, button_2, button_3, button_4)
