from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

"""
Keyboard creating factory
"""


def create_kb(text: list, cq: list, rows: int) -> object:
    """
    Func for creating keyboards inside handlers.
    """
    j = 0
    returned_kb = InlineKeyboardMarkup(row_width=rows)
    for i in text:
        button = InlineKeyboardButton(text=i, callback_data=cq[j])
        returned_kb.add(button)
        j += 1
    return returned_kb