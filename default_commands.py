from aiogram import types

"""
Setting defaults commands for bot
"""


async def set_default_commands(dp):
    await dp.bot.set_my_commands([
        types.BotCommand("start", "То, с чего все начинается 😀"),
    ])